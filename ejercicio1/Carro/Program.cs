﻿using System;

namespace Carro
{

     class Carro
    {
        //atributos
        public string marca = "Mitsubishi"; 
        public string modelo = "Outlander";
        public string version = "XS";
        public string color = "Plata";
        public string año ="2006";
        public string pasajeros = "5 pasajeros" ;

        //metodos
        public void obtenerdatos()
        {
            Console.WriteLine("Los datos del carro son:") ;
            Console.WriteLine(marca +" "+ modelo +" "+ version +" "+ color +" "+ año +" "+ pasajeros ) ;
        }

        public void acelerar()
        {
            Console.WriteLine("El carro está acelerando") ;
            
        }

        public void frenar()
        {
            Console.WriteLine("El carro se ha frenado") ;
        }

        public void apagarcarro()
        {
            Console.WriteLine("El carro se ha apagado") ;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Carro car1 = new Carro();
                      
            car1.obtenerdatos();

            car1.acelerar();

            car1.frenar();

            car1.apagarcarro();
        }
    }

   

  

}
