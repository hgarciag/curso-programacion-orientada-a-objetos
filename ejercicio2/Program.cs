﻿using System;
namespace ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
           Carro miCarro = new Carro();
           miCarro.litrosIniciales = 5;
           miCarro.color = "Rojo";
           miCarro.marca = "Chevrolet";
           miCarro.numeroPuertas = 5;
           miCarro.anyoModelo = 2013;
           miCarro.precio = 150000;
           miCarro.miMotor.asignarNumeroCilindros(4);
           miCarro.miMotor.asignarTipoCarburador("Inyeccion");
           miCarro.miMotor.TipoCombustible = "Gasolina";
           miCarro.obtenerCaracteristicas();
           miCarro.miTanque.llenarTanque((decimal)40.0+miCarro.litrosIniciales);
           miCarro.acelerar();
           miCarro.frenar();
           miCarro.acelerar();
           miCarro.frenar();
           miCarro.acelerar();
           miCarro.frenar();
           miCarro.acelerar();
           miCarro.frenar();
           miCarro.acelerar();
           miCarro.frenar();
           miCarro.acelerar();
        }
    }
}
